#include "gameol.h"
//if % was modulus, then the following expression would be field[(r%WINH)*WINW + (c%WINW)],
//but % is the reminder of the division
#define CELL(r,c) field[(r%WINH + (r<0)*WINH + (c<0)) *WINW + c%WINW]
static Uint8 field[WINH * WINW];
static int pause;

static void setCell(int r,int c) {
	 CELL(r,c)|= 0x80;
}

static Uint8 getCell(int r,int c) {
	return !!(CELL(r,c)&0x40);
}

int input(void) {
	static struct {
		int x,y,down;
	} mouse;
	static SDL_Event e;
	while(SDL_PollEvent(&e)) switch(e.type) {
		case SDL_QUIT:
			return 0;
		case SDL_MOUSEBUTTONDOWN:
			mouse.down=1;
			break;
		case SDL_MOUSEBUTTONUP:
			mouse.down=0;
			break;
		case SDL_MOUSEMOTION:
			mouse.x=e.motion.x;
			mouse.y=e.motion.y;
			break;
		case SDL_KEYDOWN:
			switch(e.key.keysym.sym) {
				case SDLK_ESCAPE:
					return 0;
				case SDLK_p:
					pause=!pause;
			}
	}
	if(mouse.down) {
		setCell(mouse.y,mouse.x);
		if(!pause) {
			setCell(mouse.y-1,mouse.x);
			setCell(mouse.y,mouse.x+1);
			setCell(mouse.y+1,mouse.x);
			setCell(mouse.y,mouse.x-1);
		}
	}
	return 1;
}

void update(void) {
	SDL_UpdateTexture(myTex,NULL,field,WINW);
	if(pause) return;
	for(int i=0;i<WINH*WINW;++i)
		field[i]>>=1;
	for(int r=0;r<WINH;++r) for(int c=0;c<WINW;++c) {
		Uint8 n=0;
		for(int i=r-1;i<=r+1;++i)
			for(int j=c-1;j<=c+1;++j)
				n+=getCell(i,j);
		if(n==3 || n-getCell(r,c)==3) setCell(r,c);
	}
}
