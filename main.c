#include "gameol.h"

static SDL_Window *myWin;

static inline int InitSubsystems(void) {
	if(SDL_Init(SDL_INIT_VIDEO)) return 0;
	myWin = SDL_CreateWindow("game of life",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			WINW*8, WINH*8, SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);
	if(myWin == NULL) return 0;
	myRen = SDL_CreateRenderer(myWin, -1, SDL_RENDERER_ACCELERATED);
	if(myRen == NULL) return 0;
	SDL_RenderSetLogicalSize(myRen, WINW, WINH);
	myTex=SDL_CreateTexture(myRen,SDL_PIXELFORMAT_RGB332,
			SDL_TEXTUREACCESS_STATIC,WINW,WINH);
	if(myTex == NULL) return 0;
	return 1;
}

int main(void) {
	if(!InitSubsystems())
		perror(SDL_GetError());
	else {
		Uint32 millis=0;
		while(input()) {
			update();
			SDL_RenderCopy(myRen,myTex,NULL,NULL);
			SDL_RenderPresent(myRen);
			if(millis>SDL_GetTicks())
				SDL_Delay(millis-SDL_GetTicks());
			millis=SDL_GetTicks()+20;
		}
	}
	if(myTex) SDL_DestroyTexture(myTex);
	if(myRen) SDL_DestroyRenderer(myRen);
	if(myWin) SDL_DestroyWindow(myWin);
	SDL_Quit();
	return 0;
}
